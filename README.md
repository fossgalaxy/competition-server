# Competition Server Code
This code runs a django based competition server.

## Developing with the server
```bash
pip install -r requirements.txt
./manage.py migrate
./manage.py createsuperuser
```

## Frontend dependencies
These are managed by bower

```bash
sudo apt-get install npm
sudo npm install -g bower
./manage.py bower install
```

## Running the server (for VMs)
```bash
screen
./manage.py runserver 0.0.0.0:8080
# press ctrl+a ctrl+d to detach
```

## Bugs
1. django-bower not in dependency list
-> fix is to pip install django-bower