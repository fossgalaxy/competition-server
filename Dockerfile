FROM python:3.5
ENV PYTHONUNBUFFERED 1

# Setup webserver
RUN pip install uwsgi

# setup enviroment
RUN adduser --disabled-password --gecos "" django
RUN mkdir /home/django/website
RUN chown -R django /home/django
RUN chmod 700 /home/django/website

# install requirements
WORKDIR /home/django/website/
ADD requirements.txt /home/django/website/
RUN pip install -r requirements.txt

# Drop to non-root and setup django
USER django

WORKDIR /home/django/website/
ADD . /home/django/website/
RUN ./manage.py collectstatic --no-input

CMD ["uwsgi", "--ini", "/home/django/website/.docker/uwsgi.ini"]
