from rest_framework import viewsets

from .models import Submission, CompetitionRun
from .serializers import SubmissionSerializer, RunSerializer

class SubmissionViewSet(viewsets.ModelViewSet):
    queryset = Submission.objects.all()
    serializer_class = SubmissionSerializer

class RunViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = CompetitionRun.objects.all()
    serializer_class = RunSerializer
