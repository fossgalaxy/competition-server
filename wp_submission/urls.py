"""competitions URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from views import CompetitionList, SubmissionEdit, SubmissionDetail, CompetitionUpload, CompetitionRunDetail, SimpleRegister, CompetitionDetail, CompetitionDashboard, CompetitionSubmit, SubmissionHistory

urlpatterns = [

    url(r'^$', CompetitionList.as_view(), name='competition_list'),
    url(r'^details/(?P<slug>[\w-]+)$', CompetitionDetail.as_view(), name='competiton_detail'),

    url(r'^run-details/(?P<pk>\d+)$', CompetitionRunDetail.as_view(), name='run_detail'),
    url(r'^sign-up/(?P<run>\d+)$', CompetitionSubmit.as_view(), name='register'),

    url(r'^db$', CompetitionDashboard.as_view(), name='dashboard'),

    url(r'^submit$', CompetitionSubmit.as_view(), name='submission_create'),
    url(r'^submit/(?P<run>\d+)$', CompetitionSubmit.as_view(), name='submission_create'),





    url(r'^submissions/(?P<pk>\d+)/edit$', SubmissionEdit.as_view(), name='submission_edit'),
    url(r'^submissions/(?P<submission>\d+)/upload$', CompetitionUpload.as_view(), name='submission_upload'),

    url(r'^r$', SimpleRegister.as_view(), name='register'),


    url(r'^submissions/v/(?P<pk>\d+)/$', SubmissionDetail.as_view(), name='submission_detail'),
    url(r'^submissions/$', SubmissionHistory.as_view(), name='submission_view'),
    url(r'^submissions/u/(?P<user>\d+)$', SubmissionHistory.as_view(), name='submission_view')
]
