from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.utils import timezone

import os

STATUS_LIST = [
    ("BP", "Build pending"),
    ("BF", "Build failed"),
    ("BS", "Build succeded")
]

class Competition(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)
    description = models.TextField(blank=True)
    specification_url = models.URLField(blank=True)
    rules_url = models.URLField(blank=True)
    samples_url = models.URLField(blank=True)
    guidelines_url = models.URLField(blank=True)

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('competiton_detail', kwargs={'slug':self.slug})

    def __unicode__(self):
        return self.name

class RunManager(models.Manager):

    def get_queryset(self):
        qs = super(RunManager, self).get_queryset()
        return qs

    def get_reg_closed(self):
        """Runs which people can no longer register for"""
        qs = super(RunManager, self).get_queryset()
        qs = qs.exclude(registration_deadline__gt=timezone.now())
        return qs

    def get_reg_open(self):
        """Runs which people can still register for"""
        qs = super(RunManager, self).get_queryset()
        qs = qs.exclude(registration_deadline__lte=timezone.now())
        return qs

class CompetitionRun(models.Model):
    name = models.CharField(max_length=100)
    competition = models.ForeignKey(Competition)
    registration_deadline = models.DateTimeField(blank=True, null=True)
    sumission_deadline = models.DateTimeField(blank=True, null=True)
    objects = RunManager()

    def is_registered(self, user):
        return self.submission_set.filter(user=user).exists()

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('run_detail', kwargs={'pk':self.pk})

    @property
    def can_register(self):
        if self.registration_deadline:
            return timezone.now() < self.registration_deadline
        else:
            return True

    def __unicode__(self):
        return "%s" % (self.name)

    class Meta:
        ordering = ["-registration_deadline", "sumission_deadline"]

class SubmissionManager(models.Manager):

    def get_queryset(self):
        qs = super(SubmissionManager, self).get_queryset()
        qs = qs.exclude(run__registration_deadline__lte=timezone.now())
        return qs


class Submission(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    run = models.ForeignKey(CompetitionRun)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    created = models.DateTimeField(auto_now_add=True)
    ranking = models.FloatField(default=1500)
    ranking_rd = models.FloatField(default=350)
    objects = SubmissionManager()

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('submission_detail', kwargs={'pk':self.pk})

    def is_owner(self, user):
        return self.user == user

    @property
    def score(self):
        return "{0} ({1})".format(self.ranking, self.ranking_rd)

    @property
    def current_upload(self):
        return self.uploads.first()

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["ranking", "ranking_rd"]
        unique_together = ( ("user","run") )


def submission_path(instance,filename):
    user_ext = os.path.splitext(filename)[1]
    return 'submissions/{0}/{1}{2}'.format(instance.submission.run.pk, instance.user.username, user_ext)

class SubmissionEntry(models.Model):
    submission = models.ForeignKey(Submission, related_name="uploads")
    status = models.CharField(max_length=5, default="BP", choices=STATUS_LIST)
    created = models.DateTimeField(auto_now_add=True)
    upload = models.FileField(upload_to=submission_path)

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('submission_detail', kwargs={'pk':self.submission.pk})

    def __unicode__(self):
        return "{1}".format(self.created, self.upload)

    class Meta:
        ordering = ["-created"]
