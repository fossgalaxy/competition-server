from django import forms
from django.core.exceptions import ValidationError

from .models import CompetitionRun, Submission, SubmissionEntry

class RegisterForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(RegisterForm, self).__init__(*args, **kwargs)

    def clean(self):
        if "run" not in self.cleaned_data:
            raise ValidationError("Invalid run provided")

        run = self.cleaned_data['run']
        if not run.can_register:
            raise ValidationError("registrations are not currently open")

        if run.is_registered(self.user):
            raise ValidationError("You already have a submission")

    class Meta:
        fields = ["name", "description", "run"]
        widgets = {'run': forms.HiddenInput()}
        model = Submission

class UploadForm(forms.ModelForm):

    class Meta:
        fields = ["submission", "upload"]
        model = SubmissionEntry
