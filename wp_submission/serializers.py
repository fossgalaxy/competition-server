from .models import Submission, SubmissionEntry, CompetitionRun
from rest_framework import serializers

class SubmissionEntrySerializer(serializers.ModelSerializer):
    upload = serializers.FileField(use_url=False)
    class Meta:
        model = SubmissionEntry
        fields = ["pk","upload", "status", "created"]

class SubmissionSerializer(serializers.ModelSerializer):
    current_upload = SubmissionEntrySerializer(read_only=True)
    class Meta:
        model = Submission
        fields = ["pk", "name", "ranking", "ranking_rd", "current_upload"]

class RunSerializer(serializers.ModelSerializer):
    submission_set = SubmissionSerializer(many=True)
    class Meta:
        fields = ["pk","name","submission_set"]
        model = CompetitionRun
