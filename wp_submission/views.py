from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView, FormView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth import get_user_model
from django import forms
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from .models import Competition, CompetitionRun, Submission, SubmissionEntry
from .forms import RegisterForm, UploadForm

# Create your views here.
class CompetitionHome(ListView):
    """Display infomation about the current competitions"""
    model = Competition
    context_object_name = "competition_list"
    template_name = "wp_submission/home.html"

class CompetitionList(ListView):
    """List the currently running competitions"""
    model = Competition
    context_object_name = "competition_list"

class CompetitionDetail(ListView):
    """List the currently running competitions"""
    model = CompetitionRun
    context_object_name = "run_list"
    paginate_by = 5

    def get_queryset(self, **kwargs):
        qs = super(CompetitionDetail, self).get_queryset(**kwargs)
        req_competition = self.kwargs.get('slug', None)
        return qs.filter(competition__slug=req_competition)

    def get_context_data(self, **kwargs):
        context = super(CompetitionDetail, self).get_context_data(**kwargs)
        req_competition = self.kwargs.get('slug', None)
        context['competition'] = get_object_or_404(Competition, slug=req_competition)
        return context

class CompetitionRunDetail(DetailView):
    """List details about a competition run"""
    model = CompetitionRun
    context_object_name = "run"

@method_decorator(login_required, name='dispatch')
class CompetitionSubmit(CreateView):
    model = Submission
    form_class = RegisterForm

    def get_form_kwargs(self):
        kwargs = super(CompetitionSubmit, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(CompetitionSubmit, self).get_context_data(**kwargs)
        run_id = self.kwargs.get('run')
        context['run'] = get_object_or_404(CompetitionRun, id=run_id)
        return context

    def get_initial(self):
        context = super(CompetitionSubmit, self).get_initial()
        context['run'] = self.kwargs.get('run', None)
        return context

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(CompetitionSubmit, self).form_valid(form)

@method_decorator(login_required, name='dispatch')
class CompetitionUpload(CreateView):
    form_class = UploadForm
    template_name = "wp_submission/submissionentry_form.html"

    def get_context_data(self, **kwargs):
        context = super(CompetitionUpload, self).get_context_data(**kwargs)
        run_id = self.kwargs.get('submission')
        context['submission'] = get_object_or_404(Submission, id=run_id)

        # if the user is not the owner of that submission, tell them off
        if not context['submission'].user == self.request.user:
            raise PermissionDenied()

        return context

    def get_initial(self):
        context = super(CompetitionUpload, self).get_initial()
        context['submission'] = self.kwargs.get('submission', None)
        return context

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(CompetitionUpload, self).form_valid(form)

@method_decorator(login_required, name='dispatch')
class SubmissionEdit(UpdateView):
    model = Submission
    fields = ["name","description"]

    def get_context_data(self, **kwargs):
        context = super(SubmissionEdit, self).get_context_data(**kwargs)
        #req_competition = self.kwargs.get('submission', None)
        #context['submission'] = get_object_or_404(Submission, id=req_user)
        return context

    def get_object(self, *args, **kwargs):
        obj = super(SubmissionEdit, self).get_object(*args, **kwargs)
        # if this user did not create the submission, tell them off
        if not obj.user == self.request.user:
            raise PermissionDenied()
        return obj

    def get_initial(self):
        context = super(SubmissionEdit, self).get_initial()
        context['run'] = self.kwargs.get('run', None)
        return context

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(SubmissionEdit, self).form_valid(form)

class SubmissionDetail(DetailView):
    """Show details about a given submission"""
    model = Submission
    context_object_name = "submission"

class SubmissionHistory(ListView):
    """A history of a given user's submissions"""
    model = Submission
    context_object_name = "submission_list"

    def get_queryset(self):
        qs = super(ListView, self).get_queryset()
        req_user = self.kwargs.get('user', None)
        if req_user:
            User = get_user_model()
            self.participant = get_object_or_404(User, id=req_user)
        else:
            self.participant = self.request.user
        return qs.filter(user=self.participant)

    def get_context_data(self, **kwargs):
        context = super(SubmissionHistory, self).get_context_data(**kwargs)
        context['participant'] = self.participant
        return context

@method_decorator(login_required, name='dispatch')
class CompetitionDashboard(ListView):
    model = CompetitionRun
    context_object_name = "run_list"
    template_name = "wp_submission/user_dashboard.html"

    def get_queryset(self):
        return Submission.objects.filter(user__pk=self.request.user.pk)

@method_decorator(login_required, name='dispatch')
class SimpleRegister(FormView):
    template_name = "wp_submission/register.html"
    form_class = RegisterForm
    success_url = reverse_lazy('dashboard')

    def get_initial(self):
        context = super(SimpleRegister, self).get_initial()
        context['run'] = self.kwargs.get('run', None)
        return context

    def form_valid(self, form):
        run = form.cleaned_data['run']
        run.participants.add(self.request.user)
        return super(SimpleRegister, self).form_valid(form)
