from django.contrib import admin
from django.apps import apps

from .models import Competition, CompetitionRun, Submission, SubmissionEntry

# Register your models here.
app = apps.get_app_config('wp_submission')
for (name,entry) in app.models.items():
    if name not in ["competitionrun", "submission"]:
        admin.site.register(entry)

class SubmissionEntryInline(admin.TabularInline):
    model = SubmissionEntry
    extra = 0
    list_display=("__unicode__", "submission", "created")

class SubmissionAdmin(admin.ModelAdmin):
    list_display = ("__unicode__","user","run")
    list_filter = ("run",)
    inlines = [SubmissionEntryInline]
admin.site.register(Submission, SubmissionAdmin)

class SubmissionInline(admin.TabularInline):
    model = Submission
    fields = ["name", "user"]
    extra = 1

class CompetitionRunAdmin(admin.ModelAdmin):
    inlines = [
        SubmissionInline
    ]
admin.site.register(CompetitionRun, CompetitionRunAdmin)
