from __future__ import unicode_literals

from django.apps import AppConfig


class WpHighscoresConfig(AppConfig):
    name = 'wp_highscores'
