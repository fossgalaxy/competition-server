from rest_framework import viewsets

from .models import ScoreTable, ScoreEntry
from .serializers import ScoreTableSerializer, ScoreEntrySerializer

class ScoreTableViewSet(viewsets.ModelViewSet):
    queryset = ScoreTable.objects.all()
    serializer_class = ScoreTableSerializer

class ScoreEntryViewSet(viewsets.ModelViewSet):
    queryset = ScoreEntry.objects.all()
    serializer_class = ScoreEntrySerializer
