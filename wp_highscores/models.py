from __future__ import unicode_literals

from django.db import models

class ScoreTable(models.Model):
    """A set of score entry records which corrispond to the competition"""
    run = models.ForeignKey('wp_submission.CompetitionRun')
    created = models.DateTimeField(auto_now_add=True)

class ScoreEntry(models.Model):
    """An individual score for a game"""
    table = models.ForeignKey(ScoreTable)
    created = models.DateTimeField(auto_now_add=True)
    submission = models.ForeignKey('wp_submission.Submission')
    score = models.FloatField()
    trace = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return "%s (%.2f)" % (self.submission, self.score)
