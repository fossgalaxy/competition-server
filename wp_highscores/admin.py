from django.contrib import admin
from django.apps import apps

app = apps.get_app_config('wp_highscores')
for (name,entry) in app.models.items():
    admin.site.register(entry)
