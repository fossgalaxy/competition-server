from rest_framework import serializers
from .models import ScoreTable, ScoreEntry

class ScoreEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = ScoreEntry

class ScoreEntry2Serializer(serializers.ModelSerializer):
    class Meta:
        model = ScoreEntry
        exclude = ["table"]

class ScoreTableSerializer(serializers.ModelSerializer):
    scoreentry_set = ScoreEntry2Serializer(many=True)

    class Meta:
        model = ScoreTable

    def create(self, validated_data):
        entries = validated_data.pop('scoreentry_set')
        table = ScoreTable.objects.create(**validated_data)
        for entry in entries:
            ScoreEntry.objects.create(table=table, **entry)
        return table
