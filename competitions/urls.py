"""competitions URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from django.conf import settings
from django.conf.urls.static import static

from wp_highscores.rest_views import ScoreTableViewSet, ScoreEntryViewSet
from wp_submission.rest_views import SubmissionViewSet, RunViewSet
from wp_submission.views import CompetitionHome

from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'submissions', SubmissionViewSet)
router.register(r'runs', RunViewSet)
router.register(r'highscore', ScoreTableViewSet)
router.register(r'highscore-entry', ScoreEntryViewSet)

urlpatterns = [
    url(r'^$', CompetitionHome.as_view(), name="home"),
    url(r'^admin/', admin.site.urls),
    url(r'^competitions/', include('wp_submission.urls')),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url('^', include('django.contrib.auth.urls'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
